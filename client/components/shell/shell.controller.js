'use strict';

angular.module('northernharvestApp')
  .controller('ShellCtrl', ['$mdSidenav', '$mdDialog', '$scope', '$location', 'authService', '$window', 'ENV', '$mdMenu', 'mixPanelFactory', function ($mdSidenav, $mdDialog, $scope, $location, authService, $window, ENV, $mdMenu, mixPanelFactory) {

    var self = this;
    
    self.principal = authService.getPrincipal();
    self.is_admin = authService.isAdmin();
    
    self.console_base_url = ENV.consoleUrl;
    
    self.first_name = self.principal ? self.principal.first_name : 'No Name';
    self.user_image = self.principal ? self.principal.image_url : null;
    
    self.logOut = function() {
      authService.logout('You have successfully logged out.');
      mixPanelFactory.track('Main-Menu:Logout');
    }
    
    self.openAdminConsole = function() {
      $window.open(self.console_base_url, '_blank');
      mixPanelFactory.track('Main-Menu:AdminConsole');
    }
   
  }]);
