'use strict';

angular.module('northernharvestApp')
  .factory('httpFactory', ['$http', 'ENV', function ($http, ENV) {

  var BASE_URL = ENV.apiUrl;
    // Public API here
    return {
      getRooms: function (capacity, floor_id, amenities) {
        var params = '';
        
        if (capacity) {
          params+= '?min_cap=' + capacity.min;
          
          if (capacity.max) {
            params+= '&max_cap=' + capacity.max;
          }
        }
        
        if (floor_id) {
          params+= params.length != 0 ? '&' : '?';         
          params+= 'floor_id=' + floor_id;
        }
        
        if (amenities && amenities.length > 0) {
          params+= params.length != 0 ? '&' : '?';
          params+= 'amenities=' + _.join(amenities, ',');
        }
        
        return $http.get(BASE_URL + 'rooms' + params);
      },
      
      getEvents: function(room_ids, selected_date) {
        
        return $http.get(BASE_URL + 'rooms/events', {
          params: {
            room_ids: room_ids,
            include_deleted: true,
            start: selected_date.startOf('day').format('YYYY-MM-DDTHH:mm:ssZ'),
            end: selected_date.endOf('day').format('YYYY-MM-DDTHH:mm:ssZ')
          }
        });
      },
      
      getRoomEvents: function(room_id, selected_date) {
        
        return $http.get(BASE_URL + 'rooms/' + room_id + '/events', {
          params: {
            include_deleted: true,
            start: selected_date.startOf('day').format('YYYY-MM-DDTHH:mm:ssZ'),
            end: selected_date.endOf('day').format('YYYY-MM-DDTHH:mm:ssZ')
          }
        });
      },
        
      createRoomEvent: function(room_id, room_event) {
        return $http.post(BASE_URL + 'rooms/' + room_id + '/events', room_event);
      },
      
      updateRoomEvent: function(room_event) {
        return $http.put(BASE_URL + 'rooms/' + room_event.room_id + '/events/' + room_event.id, room_event)
      },
      
      getAmenities: function() {
        return $http.get(BASE_URL + 'amenities');
      },
      
      getFloors: function() {
        return $http.get(BASE_URL + 'floors');
      },
      
      oAuth2Login: function(id_token, calendar_id) {
        return $http.post(BASE_URL + "auth/webapp", id_token, {
          headers: {
            "X-Auth-Method" : calendar_id
          }
        });
      }
    
    };
  }]);
