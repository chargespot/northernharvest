angular.module('northernharvestApp')
  .controller('CreateMeetingCtrl',  ['$scope', 'mdcDateTimeDialog', 'AVAILABLE_DURATION', '$mdDialog', 'params', 'httpFactory', '$timeout', 'mixPanelFactory', 'authService', function ($scope, mdcDateTimeDialog, AVAILABLE_DURATION, $mdDialog, params, httpFactory, $timeout, mixPanelFactory, authService) {
    
    var self = this;

    self.STATE_ERROR = 0;
    self.STATE_BOOKABLE = 1;
    self.STATE_BOOKING = 2;
    self.STATE_BOOKED = 3;
    self.MESSAGE_MEETING_CONFLICT = "Meeting Conflict! That time is no longer available."
    self.MESSAGE_GENERIC_ERROR = "Something awry has happened. Please try again later."
    
    self.error_msg = "";
    
    self.state = self.STATE_BOOKABLE;
    
    self.available_start_times = params.available_times;
    self.start_time = params.selected_time;
    self.date = params.selected_date;
    self.dateAsString = self.date.format('MMMM Do, YYYY');
    
    self.init = function() {
      self.is_edit = params.event ? true : false;
      self.available_duration = AVAILABLE_DURATION;        
      if (self.is_edit) {
        
        self.event = params.event;
        
        // available duration is ordered, so divide by 15 to get neareast minute interval.
        var duration_index = parseInt(moment(self.event.end).diff(moment(self.event.start), 'minutes') / 15.0);
        
        self.duration = self.available_duration[duration_index];
        
      } else {
        self.duration = self.available_duration [1];
        self.event = {
          room: {
            id: params.selected_room.id,
            name: params.selected_room.name
          },
          can_edit: true
        }
      };
    }
    
    self.init();
    
    self.close = function() {
      $mdDialog.cancel();
    }
    
    self.parseTimeToString = function(hour, minute) {
      
    }
    
    self.saveChanges = function() {
      var ampm = self.start_time.substring(5,7);
      var hour = parseInt(self.start_time.substring(0,2));
      
      //Change to military time.
      if (hour == 12) {
        if (ampm == 'am') {
          hour = 0;  
        }
      } else if (ampm == 'pm') {
        hour += 12;
      }
      
      var minute = parseInt(self.start_time.substring(3,5));
      self.event.start = moment(self.date).hour(hour).minute(minute).second(1).toDate();
      self.event.end = moment(self.date).hour(hour).minute(minute).second(0).add(self.duration.minutes, 'minutes').toDate();
      
      self.state = self.STATE_BOOKING;
      
      
      
      if (self.is_edit) {
        mixPanelFactory.track('Main-BookEvent-Edit:BookMeeting');  
        httpFactory.updateRoomEvent(self.event).then(function(response) {
          self.state = self.STATE_BOOKED;
          $timeout(function() {
            $mdDialog.hide(self.event);
          }, 600);
          mixPanelFactory.track('Main-BookEvent-Edit:BookMeeting:Success');
        }, function(error_response) {
          
          if (authService.isInvalidSessionErrorResponse(error_response)) {
            $mdDialog.hide(self.event);
          } else {
            self.state = self.STATE_ERROR;
            self.error_msg = self.MESSAGE_GENERIC_ERROR;
            mixPanelFactory.track('Main-BookEvent-Edit:BookMeeting:Failure:' + error_response.status);  
          }
        });
      } else {
        mixPanelFactory.track('Main-BookEvent-Create:BookMeeting');
        httpFactory.createRoomEvent(self.event.room.id, self.event).then(function(response){
          self.state = self.STATE_BOOKED;
          $timeout(function() {
            $mdDialog.hide(self.event);
          }, 600);
          mixPanelFactory.track('Main-BookEvent-Create:BookMeeting:Success');
        }, function(error_response) {
          self.state = self.STATE_ERROR;
        
          if (error_response.status == 409) {
            self.error_msg = self.MESSAGE_MEETING_CONFLICT;
          } else if (authService.isInvalidSessionErrorResponse(error_response)) {
            $mdDialog.hide(self.event);
          } else {
            self.error_msg = self.MESSAGE_GENERIC_ERROR;
          }
          
          mixPanelFactory.track('Main-BookEvent-Create:BookMeeting:Failure:' + error_response.status);
      });
      }
      
      
    }

  }]);
                                     
                                     
                                    