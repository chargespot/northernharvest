angular.module('northernharvestApp')
  .controller('EventInfoPanelCtrl',  ['room_event', function (room_event) {
    
  var self = this;
    
  self.parseRoomEvent = function(room_event) {
    
    if (room_event) {
      
      if (room_event.organizer_user) {
        self.full_name = room_event.organizer_user.name + ' ' + room_event.organizer_user.last_name;
      }
      
      self.summary = room_event.summary;
      self.time_range = moment(room_event.start).format('H:mma') + ' - ' + moment(room_event.end).format('H:mma');
    }
  }
    
  self.parseRoomEvent(room_event);
    
}]);
                                     
                                     
                                    