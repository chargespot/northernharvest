'use strict';

angular.module('northernharvestApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('login', {
        url: '/login',
        params: {
          message: null
        },
        templateUrl: 'app/login/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'loginCtrl',
        resolve: {
          checkLogin: ['$state','authService', '$q', function ($state, authService, $q) {
            if (authService.getPrincipal()) {
              return $q.reject().catch(function() {
                $state.go('webapp'); 
                return $q.reject();
              });
            }
          }]
        }
      });
  })