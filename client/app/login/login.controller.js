'use strict';

angular.module('northernharvestApp')
  .controller('LoginCtrl', ['$scope', 'googleFactory', '$state', 'authService', 'o365Factory', 'ENV', '$window', 'mixPanelFactory', '$stateParams', function ($scope, googleFactory, $state, authService, o365Factory, ENV, $window, mixPanelFactory, $stateParams) {

  var self = this;
  
  self.o365LogoutUrl = o365Factory.getLogoutUrl();
  self.console_register_url = ENV.consoleUrl + 'register';
    
  self.GOOGLE_REGISTER_ELEMENT_ID = "cs-google-signin"
  self.GOOGLE_REGISTER_EVENT_SUCCESS = "event:google-signin-success"
  self.GOOGLE_REGISTER_EVENT_FAILURE = "event:google-signin-failure"
  
  self.message = $stateParams.message;
    
  self.attachRegisterListener = function() {
    googleFactory.attachGoogleSignin(
        self.GOOGLE_REGISTER_ELEMENT_ID,
        self.GOOGLE_REGISTER_EVENT_SUCCESS,
        self.GOOGLE_REGISTER_EVENT_FAILURE
    );
  }
  self.attachRegisterListener();
    
  $scope.$on(self.GOOGLE_REGISTER_EVENT_SUCCESS, function (event, googleUser) {
    var userObj = {
      id_token: googleUser.getAuthResponse().id_token
    }

    self.errorMessage = '';

    mixPanelFactory.track('Login:Google:Success');
    
    authService.googleLogin(userObj).then(function(response) {
      if (response.status == 200) {
        mixPanelFactory.track('Login:Google:Success:LoggedIn');
        $state.go('webapp');    
      } else if (response.status == 401) {
        mixPanelFactory.track('Login:Google:Success:401');
        self.errorMessage = "Looks like you don't have an account, please create one above."
      } else if (response.status == 403) {
        mixPanelFactory.track('Login:Google:Success:403');
        self.errorMessage = "Oops! Looks like you don't have an account. Please create one above or contact your administrator."
      } else {
        mixPanelFactory.track('Login:Google:Success:' + response.status);
      }
    }, function(errorResponse) {
      self.message = "Something awry has happened. Please try again later.";
    });    
  });

  self.o365Login = function () {
    mixPanelFactory.track('Login:O365');
    o365Factory.login();
  }
  
  $scope.$on("adal:loginSuccess", function (event, id_token) {
    authService.o365Login(id_token).then(function(response) {
      if (response.status == 200) {
        mixPanelFactory.track('Login:O365:Success');
        $state.go('webapp');    
      } else if (response.status == 401) {
        mixPanelFactory.track('Login:O365:Success:401');
        self.errorMessage = "Looks like you don't have an account, please create one above."
      } else if (response.status == 403) {
        mixPanelFactory.track('Login:O365:Success:403');
        self.errorMessage = "Oops! Looks like you don't have an account. Please create one above or contact your administrator."
      } else {
        mixPanelFactory.track('Login:O365:Success:' + response.status);
      }
    },function(errorResponse) {
        self.message = "Something awry has happened. Please try again later.";
    });    
  });
        
  $scope.$on("adal:loginFailure", function (event, error, error_description) {
    mixPanelFactory.track('Login:O365:Success:FailureAuth');
  });
    
  self.goToRegister = function () {
    mixPanelFactory.track('Login:Trial');
    $window.open(self.console_register_url, '_blank');
  }
  
}]);
