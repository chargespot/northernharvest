'use strict';

angular.module('northernharvestApp')
    .factory('googleFactory', ['angularLoad', '$rootScope', '$state', 'ENV', 'mixPanelFactory', function (angularLoad, $rootScope, $state, ENV, mixPanelFactory) {
    
    var self = this;
        
    var WS_CLIENT_ID = ENV.gcid;

    var GOOGLE_SIGNIN_BUTTON_ID = 'cs-google-signin'; //this must match the id of the google signin button.

    var googleAuth2;
    var buttonAttached = false; //to track the attach button state.

    self.loadGoogle = function (attachSigninFn, elementId, successId, failureId) {
        angularLoad.loadScript("https://apis.google.com/js/api:client.js").then(function() {
            gapi.load('auth2', function() {
                // Retrieve the singleton for the GoogleAuth library and set up the client.
                gapi.auth2.init({
                    fetch_basic_profile: true,
                    client_id: WS_CLIENT_ID,
                    // Request scopes in addition to 'profile' and 'email'
                    scope: 'email https://www.googleapis.com/auth/calendar'
                }).then(function(response) {
                    googleAuth2 = response;
                    attachSigninFn(elementId, successId, failureId);
                }, function(errorResponse) {
                    console.log ("Could not get GoogleAuth object.")
                });

            });
        }).catch(function() {
            console.log ("Google API could not be loaded.")
        });
    }
    
    return {
        
        attachGoogleSignin: function (elementId, successId, failureId) {
            var el = document.getElementById(elementId);
            
            if (!el) {
                return;
            }

            //We need to clone this to prevent multiple onclicks from firing.  Cloning an element will remove all event listeners from the element.
            var elClone = el.cloneNode(true);
            el.parentNode.replaceChild(elClone, el);
            
            if (googleAuth2) {
                googleAuth2.attachClickHandler(elClone, {}, 
                    function(googleUser) {
                        $rootScope.$broadcast(successId, googleUser);
                    },
                    function(fail){
                        mixPanelFactory.track('Login:Google:Failure');
                        $rootScope.$broadcast(failureId, fail);
                    }
                );
                return;
            } else {
                self.loadGoogle(this.attachGoogleSignin, elementId, successId, failureId);
            }
        },
        
        consentLinkCalendar: function (success, failure){
            var options = new gapi.auth2.SigninOptionsBuilder(
                {
                    'redirect_uri': 'postmessage'
                });

            var googleUser = googleAuth2.currentUser.get();
            
            googleUser.grantOfflineAccess(options).then(success, failure);
  
        },
        
        signOut: function (success, failure) {
            
            if (googleAuth2) {
                googleAuth2.signOut().then(function(response) {
                    $state.go('login');
                }, function(errorResponse) {
                });
            } else {
                $state.go('login');
            }
            
        },
        
        disconnect: function (success, failure) {
            googleAuth2.disconnect();
        },
        
        login: function(success, failure) {
            googleAuth2.grantOfflineAccess({
                'scope': 'email https://www.googleapis.com/auth/calendar https://www.googleapis.com/auth/admin.directory.resource.calendar',
                'redirect_uri': 'postmessage'
            }).then(success, failure)
        }
     };
}]);
