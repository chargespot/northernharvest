'use strict';

describe('Directive: wsCalendar', function () {

  // load the directive's module and view
  beforeEach(module('northernharvestApp'));
  beforeEach(module('app/wsCalendar/wsCalendar.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<ws-calendar></ws-calendar>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the wsCalendar directive');
  }));
});