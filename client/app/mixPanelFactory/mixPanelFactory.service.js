'use strict';

angular.module('northernharvestApp')
    .factory('mixPanelFactory', ['ENV', function (ENV) {
    
    var ENABLED = ENV.mp;

    // Public API here
    return {
      track: function(id) {
        if (ENABLED) {
          mixpanel.track(id);  
        }
      },
      
      identify: function(email) {
        //TODO: need to call alias first, check if possible in backend
        return;
        if (ENABLED) {
          mixpanel.identify(email);
        }
      },
      
      logout: function() {
        //TODO: need to call alias first, check if possible in backend
        return;
        if (ENABLED) {
          mixpanel.reset()
        }
      }
        
    };
}]);
