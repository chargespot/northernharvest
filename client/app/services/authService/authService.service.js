'use strict';

angular.module('northernharvestApp')
    .service('authService', ['httpFactory','$localStorage', 'googleFactory', 'adalAuthenticationService', '$state', 'o365Factory', 'mixPanelFactory', function (httpFactory, $localStorage, googleFactory, adalAuthenticationService, $state, o365Factory, mixPanelFactory) {
    
    var self = this;
    self.STAFF_ID = 1;
    self.ADMIN_ID = 1;
        
    self.GOOGLE_CALENDAR_ID = 1;
    self.O365_CALENDAR_ID = 2;
    
    self.principal = {};
      
    self.isInvalidSessionErrorResponse = function(error_response) {
      return error_response.status === 401 && error_response.data && error_response.data.message == 'invalid_token';
    }
        
    self.storePrincipal = function(principal) {
      $localStorage.csPrincipal = principal;
      var user = principal.data;
      
      if (drift) {
        
        if (user) {
          drift.identify(user.id, {
            name: user.first_name ? user.first_name : '' + ' ' + user.last_name ? user.last_name : '',
            email: user.email,
            companyId: user.client.id,
            companyName: user.client.name,
            role: user.role_id == 1 ? 'Admin' : 'User'
          })  
        }
      }
      
      mixPanelFactory.identify(user.email);
    }
    
    self.getPrincipal = function () {
        if ($localStorage.csPrincipal) {
            return $localStorage.csPrincipal.data;    
        }
        
        return null;
    }
    
    self.getAuthToken = function () {
        if ($localStorage.csPrincipal) {
            return $localStorage.csPrincipal.authToken;    
        }
    }
    
    self.login = function (user) {
        return httpFactory.login(user).then(function(response) {
            //Set the user.
            self.storePrincipal(response.data)
            
            return response;
            
        }, function(errorResponse) {
            return errorResponse;
        });  
    };
        
    self.googleLogin = function(id_token) {
        
        return httpFactory.oAuth2Login(id_token, self.GOOGLE_CALENDAR_ID).then(function(response) {
            self.storePrincipal(response.data)
            return response;
        }, function(failure){
            return failure;
        }) 
    }
    
    self.o365Login = function (id_token) {
        return httpFactory.oAuth2Login({'id_token': id_token}, self.O365_CALENDAR_ID).then(function(response) {
            self.storePrincipal(response.data)
            return response;
        }, function(failure){
            return failure;
        }) 
    }   
    
    self.logout = function (message) {
      if (self.getCalendarSystemId() == self.GOOGLE_CALENDAR_ID) {
        googleFactory.signOut();
      } 

      delete $localStorage.csPrincipal;
      $state.go ('login', {'message': message});
      
      if (drift) {
        drift.reset();
      }
      
      if (mixpanel) {
        mixPanelFactory.logout();
      }
    }
    
    self.isStaff = function () {
        var principal = self.getPrincipal();
        
        if (principal) {
            return principal.client.id == self.STAFF_ID;
        }
        
        return false;
    }
    
    self.isAdmin = function() {
      var principal = self.getPrincipal ();
      
      if (principal) {
        return principal.role_id == self.ADMIN_ID;
      }
      
      return false;
    }
    
    self.getCalendarSystemId = function () {
        var principal = self.getPrincipal();
        
        if (principal) {
            return principal.calendar_system_id;
        }
        
        return self.GOOGLE_CALENDAR_ID;
    }
}]);
