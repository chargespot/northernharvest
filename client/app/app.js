'use strict';

var env = {};

if(window){  
    Object.assign(env, window.__env);
    
    if (!env.apiUrl) {
        console.error ("Could not find environment variables!")
    }
}


angular.module('northernharvestApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngAnimate',
  'ngMessages',
  'ui.router',
  'ngMaterial',
  'angularMoment',
  'ngMaterialDatePicker',
  'angularLoad',
  'ngStorage',
  'AdalAngular',
  'duScroll',
  'monospaced.mousewheel'
  
])
  .constant('ENV', env)
  .config(function ($httpProvider) {
    $httpProvider.interceptors.push(['$q', '$location', '$rootScope', '$injector', '$localStorage', function($q, $location, $rootScope, $injector, $localStorage) {
      return {
        'request': function (config) {
          config.headers = config.headers || {};
          

          if ($localStorage.csPrincipal) {
              config.headers['X-CSAuth-Token'] = $localStorage.csPrincipal.auth_token;
          }
          
          config.headers['X-Request-Origin'] = 'webapp';

          return config;

        },
        
        'responseError': function(error_response) {
          var auth_service_instance = $injector.get('authService');
          
          if(auth_service_instance.isInvalidSessionErrorResponse(error_response)) {
            auth_service_instance.logout('Your session has expired.  Please re-login.');
          }
          
          return $q.reject(error_response);
        }
      };
    }])
  })

  .config(function($mdIconProvider) {
    $mdIconProvider
      .iconSet('action', '../assets/iconsets/action-icons.svg', 24)
      .iconSet('alert', '../assets/iconsets/alert-icons.svg', 24)
      .iconSet('av', '../assets/iconsets/av-icons.svg', 24)
      .iconSet('communication', '../assets/iconsets/communication-icons.svg', 24)
      .iconSet('content', '../assets/iconsets/content-icons.svg', 24)
      .iconSet('device', '../assets/iconsets/device-icons.svg', 24)
      .iconSet('editor', '../assets/iconsets/editor-icons.svg', 24)
      .iconSet('file', '../assets/iconsets/file-icons.svg', 24)
      .iconSet('hardware', '../assets/iconsets/hardware-icons.svg', 24)
      .iconSet('icons', '../assets/iconsets/icons-icons.svg', 24)
      .iconSet('image', '../assets/iconsets/image-icons.svg', 24)
      .iconSet('maps', '../assets/iconsets/maps-icons.svg', 24)
      .iconSet('navigation', '../assets/iconsets/navigation-icons.svg', 24)
      .iconSet('notification', '../assets/iconsets/notification-icons.svg', 24)
      .iconSet('social', '../assets/iconsets/social-icons.svg', 24)
      .iconSet('toggle', '../assets/iconsets/toggle-icons.svg', 24)
      .iconSet('avatar', '../assets/iconsets/avatar-icons.svg', 128);
  })
  .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider
      .otherwise('/login');

    $locationProvider.html5Mode(true);
  })

  .config(['adalAuthenticationServiceProvider', function (adalProvider) {
  adalProvider.init(
    {
      instance: 'https://login.microsoftonline.com/', 
      clientId: 'e34abacc-1ac9-492c-871c-adcc62902c00', //Login
      popUp: true
    });
  }])

  .config(function($mdThemingProvider) {
    $mdThemingProvider.definePalette('wsgreen', {
      '50': 'f1f8e8',
      '100': 'dceec5',
      '200': 'c5e29f',
      '300': 'aed679',
      '400': '9cce5c',
      '500': '8bc53f',
      '600': '83bf39',
      '700': '78b831',
      '800': '6eb029',
      '900': '5ba31b',
      'A100': 'eaffda',
      'A200': 'ceffa7',
      'A400': 'b1ff74',
      'A700': 'a3ff5b',
      'contrastDefaultColor': 'light',
      'contrastDarkColors': [
        '50',
        '100',
        '200',
        '300',
        '400',
        '500',
        '600',
        '700',
        '800',
        'A100',
        'A200',
        'A400',
        'A700'
      ],
      'contrastLightColors': [
        '900'
      ]
    });
  
    $mdThemingProvider.definePalette('wsaltgreen', {
      '50': 'f4f9ed',
      '100': 'e3f1d1',
      '200': 'd1e7b3',
      '300': 'bfdd94',
      '400': 'b1d67d',
      '500': 'a3cf66',
      '600': '9bca5e',
      '700': '91c353',
      '800': '88bd49',
      '900': '77b238',
      'A100': 'fefffd',
      'A200': 'e5ffca',
      'A400': 'cbff97',
      'A700': 'beff7e',
      'contrastDefaultColor': 'light',
      'contrastDarkColors': [
        '50',
        '100',
        '200',
        '300',
        '400',
        '500',
        '600',
        '700',
        '800',
        '900',
        'A100',
        'A200',
        'A400',
        'A700'
      ],
      'contrastLightColors': ['900']
    });

    $mdThemingProvider.definePalette('wsred', {
      '50': 'fce9e8',
      '100': 'f8c9c6',
      '200': 'f4a5a1',
      '300': 'ef807b',
      '400': 'eb655e',
      '500': 'e84a42',
      '600': 'e5433c',
      '700': 'e23a33',
      '800': 'de322b',
      '900': 'd8221d',
      'A100': 'ffffff',
      'A200': 'ffdcdb',
      'A400': 'ffaaa8',
      'A700': 'ff918f',
      'contrastDefaultColor': 'light',
      'contrastDarkColors': [
        '50',
        '100',
        '200',
        '300',
        '400',
        'A100',
        'A200',
        'A400',
        'A700'
      ],
      'contrastLightColors': [
        '500',
        '600',
        '700',
        '800',
        '900'
      ]
    });

  
    $mdThemingProvider.theme('default')
      .primaryPalette('wsgreen')
      .accentPalette('wsaltgreen', {
        'default': '500',
      })
      .warnPalette('wsred');
    
    
  });








