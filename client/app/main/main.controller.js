'use strict';

angular.module('northernharvestApp')
  .controller('MainCtrl', ['$scope', '$http', 'moment', '$document', '$window', '$location',  '$mdDialog', 'mdcDateTimeDialog', 'AVAILABLE_TIME_SLOTS', 'SELECTABLE_TIME_SLOTS', 'httpFactory', 'rooms', '$mdPanel', 'floors', 'amenities', '$timeout', 'mixPanelFactory', 'ENV', function ($scope, $http, moment, $document, $window, $location, $mdDialog, mdcDateTimeDialog, AVAILABLE_TIME_SLOTS, SELECTABLE_TIME_SLOTS, httpFactory, rooms, $mdPanel, floors, amenities, $timeout, mixPanelFactory, ENV) {

  var self = this;
    
  self.HOURS_IN_A_DAY = 24;
  self.TIME_SLOTS_PER_HOUR = 4;
    
  //for polling
  self.POLLING_TIME_MILLI = ENV.pt;
  self.error_count = 0;
  self.polling_promise;
    
  //scrolling options
  self.SCROLL_LF_OFFSET = 600;  
  
  self.scroll_x_position = 0;
  self.scroll_y_position = 0;
    
  self.show_scroll_right = true;
  self.show_scroll_down = true;
    
  self.mdPanel = $mdPanel;
    
  self.available_rooms = rooms.data;
  self.available_floors = floors.data.data;
  self.available_amenities = amenities.data.data;
    
  self.available_rooms_length = self.available_rooms.length;
  self.available_rooms_mapped = new Map();
  
  //static elements
  self.calendar_container = angular.element(document.getElementById('calendar-layout'));
  self.unwrapped_calendar_container = self.calendar_container[0];
  self.time_container = angular.element(document.getElementById('ws-time-container'));
  self.room_header_container = angular.element(document.getElementById('ws-rooms-header-container'));

  
  self.WS_SELECTED_SLOT_CLASS = 'ws-room-event-slot-';
    
    self.CAPACITY_ALL = {
      id: -1,
      name: 'All'
    }

    self.FLOOR_ALL = {
      id: -1,
      name: 'All'
    }

    self.AMENITIES_ALL = {
      id: -1,
      name: 'All'
    }
    
    self.OPTIONS_AVAILABILITY = [
    {id: 1, name: 'Available'},
    {id: 2, name: 'Unavailable'},
    {id: 3, name: 'All'}
  ]
  
  self.OPTIONS_CAPACITY = [
    {title:'1-10', min: 1, max: 10},
    {title:'11-20', min: 11, max: 20},
    {title:'21-30', min: 21, max: 30},
    {title:'30+', min: 30},
    
  ];
    
  self.loading_dialog_config = {
    templateUrl: 'app/templates/loading.html',
    parent: angular.element(document.body),
    clickOutsideToClose:false,
    escapeToClose:false
  }
  
  
  self.available_time_slots = AVAILABLE_TIME_SLOTS;
  
  self.availability;
  self.capacity;
  self.floor;
  self.amenities;
  self.time_slots = [];
  
  self.min_date = moment().startOf('day');
  self.max_date = moment().add(2, 'weeks').format('YYYY-MM-DD');
  self.selected_date = moment();

    
  var options = {
    duration: 700,
    easing: 'easeInQuad',
    offset: 120,
    callbackBefore: function(element) {
        console.log('about to scroll to element', element);
    },
    callbackAfter: function(element) {
        console.log('scrolled to element', element);
    }
}
  
  self.mapRooms = function() {
    
    self.available_rooms.forEach(function(room) {
      room.events_mapped = new Map(); //existing events mapped for easy access.
      room.new_event_timeslots = []; //new events to be synced.
      self.available_rooms_mapped.set (room.id, room);
    });
  }
  
  self.nextDay = function() {
    self.selected_date = self.selected_date.add(1, 'days');
    self.filterRooms();
    mixPanelFactory.track('Main:NextDay');
  }
  
  self.previousDay = function() {
    self.selected_date = self.selected_date.subtract(1, 'days');
    self.filterRooms();
    mixPanelFactory.track('Main:PreviousDay');
  }
  
  self.previousDayDisabled = function() {
    return self.selected_date.diff(self.min_date, 'days') < 1 ;
  }
  
  self.resetHScroll = function () {
    self.calendar_container.scrollLeft(0);   
    self.room_header_container.scrollLeft(0);
    
    self.update_h_scroll();
  }
   
  self.gotoWorkingHoursOrLater = function() {
    
    var working_hour = 8;
    
    var start_hour_div = angular.element(document.getElementById('div-8am'));
    
    // TODO: Use actual working hours.
    if (self.isSelectedDateToday()) {
        
      var now_moment = moment();
      
      if (now_moment.hour() > 10) {
        start_hour_div = angular.element(document.getElementById('div-' + now_moment.hour(now_moment.hour() - 2).format('ha')));
      }
    }
    
    self.calendar_container.scrollTo(start_hour_div);   
    self.time_container.scrollTo(start_hour_div);   
    
    self.update_h_scroll();
    self.update_v_scroll();
    
  }
  
  self.scrollHorizontal = function (is_left) {
    
    if (is_left) {
      self.calendar_container.scrollLeft(self.calendar_container.scrollLeft() - self.SCROLL_LF_OFFSET);   
      self.room_header_container.scrollLeft(self.room_header_container.scrollLeft() - self.SCROLL_LF_OFFSET); 
      mixPanelFactory.track('Main:ScrollLeft');
    } else {
      self.calendar_container.scrollLeft(self.calendar_container.scrollLeft() + self.SCROLL_LF_OFFSET);   
      self.room_header_container.scrollLeft(self.room_header_container.scrollLeft() + self.SCROLL_LF_OFFSET); 
      mixPanelFactory.track('Main:ScrollRight');
    }
    
    self.update_h_scroll();
    
  }
  
  self.update_h_scroll = function () {
    self.scroll_x_position = self.calendar_container.scrollLeft();
    self.show_scroll_right = (self.unwrapped_calendar_container.scrollLeft + self.unwrapped_calendar_container.offsetWidth) != self.unwrapped_calendar_container.scrollWidth;
  }
  
  self.scrollVertical = function (is_up) {
    if (is_up) {
      self.calendar_container.scrollTop(self.calendar_container.scrollTop() - 200);   
      self.time_container.scrollTop(self.time_container.scrollTop() - 200);   
      mixPanelFactory.track('Main:Earlier');
    } else{
      self.calendar_container.scrollTop(self.calendar_container.scrollTop() + 200);   
      self.time_container.scrollTop(self.time_container.scrollTop() + 200);  
      mixPanelFactory.track('Main:Later');
    }
    
    self.update_v_scroll();
 
  }
  
  self.update_v_scroll = function() {
    self.scroll_y_position = self.calendar_container.scrollTop();
    self.show_scroll_down = (self.unwrapped_calendar_container.scrollTop + self.unwrapped_calendar_container.offsetHeight) < self.unwrapped_calendar_container.scrollHeight;
  }
  
  self.mouseScroll = function(event, delta, deltaX, deltaY) {
    
    //ignore horizontal scrolls.
    if (deltaX != 0) {
      return;
    }
    
    var scroll_amount = deltaY < 0 ? -64 : 64; 
    self.calendar_container.scrollTop(self.calendar_container.scrollTop() - (scroll_amount));   
    self.time_container.scrollTop(self.time_container.scrollTop() - (scroll_amount)); 
    
    self.update_v_scroll();
    event.preventDefault();
  }

  self.showBusy = function() {
    $mdDialog.show(self.loading_dialog_config);
  }
    
  self.stopShowBusy = function() {
    $mdDialog.hide();
  }
  
  self.initAvailableTimeSlots = function() {
    
    self.selectable_time_slots = new Array(self.HOURS_IN_A_DAY);
    
    for (var a = 0; a < self.HOURS_IN_A_DAY; a ++) {
      self.selectable_time_slots[a] = new Array(self.TIME_SLOTS_PER_HOUR);
      for (var b = 0; b < self.TIME_SLOTS_PER_HOUR; b++) {
        self.selectable_time_slots[a][b] = moment().hours(a).minutes(b * 15).format('hh:mma')
      }
    }
    
    self.flattened_time_slots = _.flatten(self.selectable_time_slots);
    self.selectable_time_slots_length = self.flattened_time_slots.length;
  }
  
  self.buildRoomTimeSlots = function () {
    for (var i = 0; i < self.available_rooms_length; i++) {
      
      var current_room = self.available_rooms[i];
      current_room.time_slots = [];
      
      for (var a = 0; a < self.HOURS_IN_A_DAY; a ++) {
        for (var b = 0; b < self.TIME_SLOTS_PER_HOUR; b++) {
          current_room.time_slots.push({
            display: '',
            summary: '',
            hour: a,
            minute: b * 15,
            can_edit: true,
            time: self.selectable_time_slots[a][b],
            css_class: '',
            css_style: ''
          });  
        }
      }
    }
  }
  
  self.showDatePicker = function() {
    mdcDateTimeDialog.show({
      multiple: true,
      minDate: self.min_date,
      maxDate: self.max_date,
      currentDate: self.selected_date.format('YYYY-MM-DD'),
      format: 'YYYY-MM-DD',
      time: false
    }).then(function (date) {
      self.selected_date = moment(date);
      self.filterRooms();
    });
    mixPanelFactory.track('Main:Calendar');
  }

  self.timeSlotOnEnter = function(room, time_slot, index) {
    
    if (time_slot.room_event) {
      if (time_slot.can_edit) {
        time_slot.display = 'Edit';
      }
    } else {
      time_slot.display = 'Book a Meeting';
      time_slot.css_class = 'ws-timeslot-selected';
    }
  }
  
  self.timeSlotOnLeave = function(room, time_slot, index) {
    if (!time_slot.can_edit) {
      return;
    }
    
    if (time_slot.room_event) {
      time_slot.display = time_slot.room_event.summary;  
    } else {
      time_slot.display = '';
      time_slot.css_class = '';
    }
  }
  
  self.timeSlotOnClick = function(room, time_slot, ev, index) {
    if (!time_slot.can_edit) {
      return;
    }
    
    
    //Calculate earliest bookable time.
    var moment_current_time = moment();
    var moment_start_day = moment().startOf('day');
    var start_index = 0;
    
    if (self.isSelectedDateToday()) {
      var date_string = '';
      
      start_index = _.indexOf (self.flattened_time_slots, moment_current_time.minute(parseInt(moment_current_time.minute() / 15.0) * 15).format('hh:mma'));
    }    
    
    //Default config for create/edit meeting window.
    var dialog_config = {
      controller: 'CreateMeetingCtrl',
      controllerAs: 'createMeetingCtrl',
      templateUrl: 'app/templates/createMeeting.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:false,
      multiple: false,
      escapeToClose:true
    }
    
    //EDIT.
    if (time_slot.room_event) {
      mixPanelFactory.track('Main:EditMeeting');
      //Show error dialog if room doesn't have an id yet (means it was just created).
      if (!time_slot.room_event.id) {
        $mdDialog.show(
          $mdDialog.alert()
            .parent(angular.element(document.querySelector('#popupContainer')))
            .clickOutsideToClose(true)
            .title('Still Syncing')
            .textContent('Our servers are still catching up, please try again in a minute.')
            .ariaLabel('Still Syncing')
            .ok('Got it!')
            .targetEvent(ev)
        );
        mixPanelFactory.track('Main:EditMeeting:Unsynced');

        return;
      }
      
      dialog_config.locals = {
        params: {
          selected_date: self.selected_date,
          selected_time: time_slot.time,
          selected_room: room,
          available_times: _.slice(
              self.flattened_time_slots, 
              start_index, 
              index + 32 > self.selectable_time_slots_length ? self.selectable_time_slots_length : index + 32 //Cannot be more than the next day for now.
            ),
          available_rooms: self.available_rooms,
          event: time_slot.room_event
        }
      };
      $mdDialog.show(dialog_config).then(function(meeting) {
        self.resetTimeSlot(room, time_slot);
        self.addRoomEvent(room, meeting);
        self.roomReload(room.id);
      }, function() {
        console.log("Cancelled.")
      });    
    } else {
      dialog_config.locals = {
          params: {
            selected_date: self.selected_date,
            selected_time: time_slot.time,
            selected_room: room,
            available_times: _.slice(
              self.flattened_time_slots, 
              start_index, 
              index + 32 > self.selectable_time_slots_length ? self.selectable_time_slots_length : index + 32 //Cannot be more than the next day for now.
            ),

            available_rooms: self.available_rooms
          }
        };
      
      $mdDialog.show(dialog_config).then(function(meeting) {
        self.addRoomEvent(room, meeting, true);
        self.roomReload(room.id);
      }, function() {
        console.log("Cancelled.")
      });    
      
      mixPanelFactory.track('Main:NewMeeting');
    }
  }
  
  self.getEvents = function(is_silent_refresh) {
    
    //no results.
    if (self.available_rooms_length == 0) {
      if (!is_silent_refresh) {
        self.stopShowBusy();
      }
    }

    httpFactory.getEvents(_.map(self.available_rooms,'id').join(','), self.selected_date).then(function(response) {
      if (response.status == 200) {
        var rooms_data = response.data;
        var length = rooms_data.length;

        for (var a = 0; a < length; a++) {

          var room_data = rooms_data[a];
          
          if (!room_data.room_id) {
            continue;
          }
          
          var room = self.available_rooms_mapped.get(room_data.room_id);
          var events = room_data.events;
          
          //remove all new events created. They will be re-added 
          room.new_event_timeslots.forEach(function(time_slot) {
            self.resetTimeSlot(room, time_slot);
          })

          events.forEach(function(room_event) {
            var time_slot_index = room.events_mapped.get(room_event.id);

            //TODO - version events?
            if (time_slot_index) {
              var time_slot = room.time_slots[time_slot_index]

              //Event has been deleted, so remove it from timeslot.
              if (room_event.deleted) {
                self.resetTimeSlot(room, time_slot);
              } else {

                //Event has been updated, so update the timeslot event.
                if (room_event.last_updated != time_slot.room_event.last_updated) {
                  self.resetTimeSlot(room, time_slot);
                  self.addRoomEvent(room, room_event);
                }  
              }
            } else {

                //No room event exists for this time slot, so create a new room event.
                self.addRoomEvent(room, room_event);
            }
          });

          if (!is_silent_refresh) {
            self.gotoWorkingHoursOrLater();  
            self.resetHScroll();
          }

          self.error_count = 0;
          self.nextfullReload();
        }
      }

      if (!is_silent_refresh) {
        self.stopShowBusy();  
      }

    }, function(errorResponse) {

      if (!is_silent_refresh) {
        self.stopShowBusy();  
      }
      self.nextfullReload(++self.error_count * 2 * self.POLLING_TIME_MILLI);
    });

  }
  
  self.resetTimeSlot = function(room, time_slot) {
    
    if (time_slot.room_event && time_slot.room_event.id) {
      room.events_mapped.delete(time_slot.room_event.id)
    }
    
    time_slot.css_class = '';
    time_slot.css_style = '';
    time_slot.summary = '';
    time_slot.display = '';
    time_slot.can_edit = true;
    time_slot.room_event = undefined;
  }
  
  //BAM
  //TODO: fIX problem with times not ending a
  self.addRoomEvent = function(room, room_event) {

    var start_moment = moment(room_event.start);
    var end_moment = moment(room_event.end);

    var start_hour = start_moment.hour();
    var start_minute = start_moment.minute();

    if (start_minute >= 45) {
      start_minute = 45;
    } else if (start_minute >= 30) {
      start_minute = 30;
    } else if (start_minute >= 15) {
      start_minute = 15;
    } else {
      start_minute = 0;
    }

    var minutes_diff = end_moment.diff(start_moment, 'minutes');
    
    var time_slot_index = _.findIndex(room.time_slots, function(time_slot) { 
      return time_slot.hour == start_hour && time_slot.minute == start_minute;
    });
    
    if (time_slot_index != -1) {
      var time_slot = room.time_slots[time_slot_index];

      var height = (Math.ceil(minutes_diff / 15.0) * 16) - 1;

      var time_slot_selected_class = self.WS_SELECTED_SLOT_CLASS + room_event.id;

      if (room_event.can_edit) {
        time_slot.css_class = 'ws-timeslot-booked-organizer ' + time_slot_selected_class;
        time_slot.display = room_event.summary;
        time_slot.can_edit = true;
      } else {
        time_slot.css_class = 'ws-timeslot-booked-not-organizer ' + time_slot_selected_class;
        time_slot.can_edit = false;
        time_slot.display = '';
      }

      time_slot.room_event = room_event;
      
      //Save reference of room event timeslot to update later.
      if (room_event.id) {
        room.events_mapped.set(room_event.id, time_slot_index);  
      } else {
        room.new_event_timeslots.push(time_slot);
      }
      
      time_slot.css_style = {
        'height': height + 'px',
        'line-height': height + 'px'
      }
    }
  }
  
  self.favouriteClicked = function() {
    mixPanelFactory.track('Main:Favourite');
  }
  
  self.capacityClicked = function() {
    mixPanelFactory.track('Main:FilterCapacity')
  }
  
  self.amenitiesClicked = function() {
    mixPanelFactory.track('Main:FilterAmenities')
  }
  
  self.floorClicked = function() {
    mixPanelFactory.track('Main:FilterFloor')
  }

  self.showMoreInfo = function(ev, room) {
    var panelPosition = $mdPanel.newPanelPosition()
    .relativeTo('.ws-room-' + room.id)
    .addPanelPosition($mdPanel.xPosition.CENTER, $mdPanel.yPosition.BELOW);
    
    var panelAnimation = $mdPanel.newPanelAnimation().withAnimation($mdPanel.animation.SLIDE).duration(1000);


    var config = {
      attachTo: angular.element(document.body),
      controller: 'MoreInfoPanelCtrl',
      controllerAs: 'ctrl',
      templateUrl: 'app/templates/moreInfoPanel.html',
      panelClass: 'ws-room-more-info-panel',
//      animation:panelAnimation,
      position: panelPosition,
      locals: {
        'amenities': room.amenities,
      },
      openFrom: ev,
      clickOutsideToClose: true,
      escapeToClose: true,
      focusOnOpen: false,
      zIndex: 11
    };

    $mdPanel.open(config);
    mixPanelFactory.track('Main:RoomInfo');
  }
  
  self.getHistoryStyle = function() {
    var moment_current_time = moment();
    
    if (!self.isSelectedDateToday()) {
      return {};
    }
    
    var hour_multiplier = moment_current_time.hour() * 4;
    var minute_multiplier = parseInt(moment_current_time.minute() / 15.0);
    
    return {
      'height': ((hour_multiplier + minute_multiplier) * 16) + 'px',
      'width': 200 * self.available_rooms.length + 'px'
    }
  }
  
  self.isSelectedDateToday = function() {
    return self.selected_date.startOf('day').diff(moment().startOf('day'), 'days') == 0;
  }
  
  self.formatTimeRange = function(room_event) {
    return moment(room_event.start).format('hh:mma') + ' - ' + moment(room_event.end).format('hh:mma');
  }
  
  self.formatOrganizerName = function(room_event) {
    
    if (room_event) {
      if (room_event.ws_organizer_user) {
        return room_event.ws_organizer_user.name + ' ' + room_event.ws_organizer_user.last_name;
      } 
      
      if (room_event.organizer_user) {
        return room_event.organizer_user.name + ' ' + room_event.organizer_user.last_name;
      }
    }
    
    return 'Unknown';
  }
  
  self.applyFilters = function() {
    self.filterRooms();
    mixPanelFactory.track('Main.ApplyFilters');
  }
  
  self.filterRooms = function() {
    self.showBusy();
    httpFactory.getRooms(self.capacity, self.floor, self.amenities).then(function(response) {
      self.available_rooms = response.data;
      self.available_rooms_length = self.available_rooms.length;
      self.loadRooms();
      
    }, function(error_response) {
      self.stopShowBusy();
    });
  }
  
  
  //HACK to remove ALL checkbox from amenities filter.
  self.removeAllFromAmenityFilter = function() {
    self.amenities = _.remove(self.amenities, function(amenity) {
      return amenity != '';
    })
  }
  
  self.clearFilters = function () {
    self.capacity = '';
    self.floor = '';
    self.amenities = []; 
    
    self.filterRooms();
    
    mixPanelFactory.track('Main:ClearFilters');
  }
  
  
  self.showBusy();
  self.initAvailableTimeSlots();
  
  self.loadRooms = function() {
    self.buildRoomTimeSlots();
    self.mapRooms();
    self.getEvents(false);
  }  
    
  self.cancelNextLoad = function(promiseToCancel) {
    $timeout.cancel(promiseToCancel);
  };
    
  self.nextfullReload = function(mill) {
    mill = mill || self.POLLING_TIME_MILLI;

    //Always make sure the last timeout is cleared before starting a new one
    self.cancelNextLoad(self.fullReloadPromise);
    self.fullReloadPromise = $timeout(function() {
      self.getEvents(true)
    }, mill);
  };
    
  self.roomReload = function(room_id) {
    self.cancelNextLoad(self.roomReloadPromise);
    self.roomReloadPromise = $timeout(function() {
      self.getRoomEvents(room_id)
    }, 5000);
  }
  
  self.getRoomEvents = function(room_id) {
    httpFactory.getRoomEvents(room_id, self.selected_date).then(function(response) {
      if (response.status == 200) {

        var events = response.data;
        var length = events.length;
        var room_id = parseInt(response.config.url.match(/rooms\/(\d+)\/events/, "gi")[1])
        var room = self.available_rooms_mapped.get(room_id);

        //remove all new events created. They will be re-added 
        room.new_event_timeslots.forEach(function(time_slot) {
          self.resetTimeSlot(room, time_slot);
        })

        events.forEach(function(room_event) {
          var time_slot_index = room.events_mapped.get(room_event.id);

          //TODO - version events?
          if (time_slot_index) {
            var time_slot = room.time_slots[time_slot_index]

            //Event has been deleted, so remove it from timeslot.
            if (room_event.deleted) {
              self.resetTimeSlot(room, time_slot);
            } else {

              //Event has been updated, so update the timeslot event.
              if (room_event.last_updated != time_slot.room_event.last_updated) {
                self.resetTimeSlot(room, time_slot);
                self.addRoomEvent(room, room_event);
              }  
            }
          } else {

              //No room event exists for this time slot, so create a new room event.
              self.addRoomEvent(room, room_event);
          }
        });
      }
    }, function(errorResponse) {
      
    });
  }
    
  //Always clear the timeout when the view is destroyed, otherwise it will keep polling and leak memory
  $scope.$on('$destroy', function() {
    self.cancelNextLoad(self.fullReloadPromise);
    self.cancelNextLoad(self.roomReloadPromise);
  });

  self.loadRooms();
}]);
