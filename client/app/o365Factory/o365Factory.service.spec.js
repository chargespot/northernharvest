'use strict';

describe('Service: o365Factory', function () {

  // load the service's module
  beforeEach(module('bluelabelApp'));

  // instantiate service
  var o365Factory;
  beforeEach(inject(function (_o365Factory_) {
    o365Factory = _o365Factory_;
  }));

  it('should do something', function () {
    expect(!!o365Factory).toBe(true);
  });

});
