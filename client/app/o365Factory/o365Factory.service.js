'use strict';

angular.module('northernharvestApp')
    .factory('o365Factory', ['adalAuthenticationService', 'ENV', '$sce', function (adalAuthenticationService, ENV, $sce) {
    
    var PERMISSIONS_LOGIN_CLIENT_ID = ENV.olid;
    var PERMISSIONS_CLIENT_ID = ENV.ooid;
        
    var LOGOUT_URL = 'https://login.microsoftonline.com/common/oauth2/logout';

    // Public API here
    return {
        login: function() {
            adalAuthenticationService.config.clientId = PERMISSIONS_LOGIN_CLIENT_ID;
            adalAuthenticationService.login();
        },

        getLogoutUrl: function() {
            return $sce.trustAsResourceUrl(LOGOUT_URL);
        },
        
        requestOfflineAccess: function() {
            adalAuthenticationService.config.clientId = PERMISSIONS_CLIENT_ID;
            adalAuthenticationService.login('code id_token');
        }
        
    };
}]);
